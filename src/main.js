const TEST_CONFIG = {
  id: 'demo',
  title: "共享单车",
  appkey: "56a0a88c4407a3cd028ac2fe",
  baseUrl:'http://parkinglock.yunba.io'
  // baseUrl:'http://localhost:9031'
};
try {
  window.APP_CONFIG = JSON.parse(window.APP_CONFIG);
} catch(e) {
  window.APP_CONFIG = TEST_CONFIG;
}

window.APP_KEY = APP_CONFIG.appkey;
window.UPLOAD_URL= APP_CONFIG.uploadUrl;
window.UPLOAD_BASE_URL = APP_CONFIG.uploadBaseUrl;
window.BASE_URL = APP_CONFIG.baseUrl;
window.USER_CACHE_KEY = APP_CONFIG.id + '_ADMIN';

window.IS_LOGIN = !!localStorage.getItem(USER_CACHE_KEY);

// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import App from './App';
import router from './router';

import iview from 'iview';
import 'iview/dist/styles/iview.css';
import './mytheme/index.less';

import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';

import VueI18n from 'vue-i18n';

import messages from '../static/language/index';

import axios from 'axios';

import NProgress from 'nprogress';

// vue-resource 
import Resource from 'vue-resource';
Vue.use(Resource);

//公共layout样式
import LayoutMain from '@/components/common/layout-main';

import Highcharts from 'vue2-highcharts';
// import HighchartsMore from 'vue-highcharts/highcharts-more.js'


// highchart
// HighchartsMore(Highcharts);
Vue.use(Highcharts);
Vue.component('vue-highcharts',Highcharts);
// vue-i18n
Vue.use(VueI18n);

Vue.locale=()=>{};

var lang = ()=>{
  // localStorage language
  let t = window.localStorage.getItem('language');
  if(t){
    return t;
  }else{
    return 'zh';
  }
}
var language = lang();
window.language = lang();

const i18n = new VueI18n({
  locale:language,
  messages
})

// iview
Vue.use(iview,{
  i18n:(key,value) => i18n.t(key,value)
});

// element-ui
Vue.use(ElementUI,{
  i18n:(key,value) => i18n.t(key,value)
});



// moment format
window.TIME_ZONE = 'Asia/Shanghai';
window.moment = require('moment-timezone');


Vue.filter('time-format', function(timestamp, format, timezone){
  if(timestamp){
    let pow = timestamp.toString().length - 13;
    timestamp = timestamp/Math.pow(10,pow);
    format = format || 'YYYY-MM-DD HH:mm:ss';
    timezone = timezone || TIME_ZONE;
    return moment(timestamp).tz(timezone).format(format);
  }
  return null;
})


// $http request custom

var httpRequest = axios.create({
  baseURL:BASE_URL
})

httpRequest.interceptors.request.use(
  function (config) {
    NProgress.start();
    const token = localStorage.getItem(USER_CACHE_KEY);
    if(token){
      config.headers['Authorization'] = `${token}`;
    }
    return config;
  },
  function (error) {
    console.log(error);
    return Promise.reject(error);
  }
)

httpRequest.interceptors.response.use(
  function (response) {
    NProgress.done();
    return response;
  },
  function (error) {
    return Promise.reject(error);
  }
)

Vue.prototype.httpRequest=httpRequest;

Vue.config.productionTip = false

/* eslint-disable no-new */
window.Vue = Vue;
Vue.component('yb-layout',LayoutMain);
// Vue.component('chart',chart);
window.vm = new Vue({i18n}).$mount('#app');

new Vue({
  el: '#app',
  router,
  i18n,
  template: '<App/>',
  components: { App }
})
