import Vue from 'vue'
import router from 'vue-router'
import Login from '@/components/login'
import Register from "@/components/register/register"
import Main from '@/components/main'
import Dashboard from '@/components/dashboard'
import ParkingLockManagement from '@/components/parkingLockManagement/list'
import Users from '@/components/users/list'
import Custom from '@/components/custom/list'
import Finance from '@/components/finance/list'
import Region from '@/components/region/list'
import Content from '@/components/content/list'
import Advertisement from '@/components/advertisement/list'
import Setting from '@/components/setting/list'

Vue.use(router)

export default new router({
  // mode:'history',
  routes: [
    {
      path:'/login',
      name:'Login',
      component:Login,
      beforeEnter:(to,from,next)=>{
        if(window.IS_LOGIN){
          return next({
            path:'/'
          })
        }
        next();
      }
    },
    {
      path:'/register',
      name:'register',
      component:Register
    },
    {
      path: '/',
      name: 'main',
      component: Main,
      children:[
        {
          path:'',
          name:'dashboard',
          component:Dashboard
        },
        {
          path:'/parkingLockManagement',
          name:'parkingLockManagement',
          component:ParkingLockManagement
        },
        {
          path:'/setting',
          name:'setting',
          component:Setting
        },
        {
          path:'/users',
          name:'users',
          component:Users
        },
        {
          path:'/finance',
          name:'finance',
          component:Finance
        },
        {
          path:'/advertisement',
          name:'advertisement',
          component:Advertisement
        },
        {
          path:'/content',
          name:'content',
          component:Content
        },
        {
          path:'/regionManagement',
          name:'regionManagement',
          component:Region
        },
        {
          path:'/customServer',
          name:'customServer',
          component:Custom
        }
      ],
      beforeEnter:(to,from,next)=>{
        if(!window.IS_LOGIN){
          return next({
            path:'/login'
          })
        }
        next();
      }
    }
  ]
})
