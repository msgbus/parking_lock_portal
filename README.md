# parklock_management

> 项目使用vue.js + iview + element_ui

* node版本 > 7.4

## Build Setup

``` bash
# 安装依赖
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

```
## 部署

* 部署服务器：abj-elogic-test1

* 部署目录：~/parking_lock_server/client

1、进入~/parking_lock_server/client

`git pull [url]`

2、安装依赖

`export PATH=/opt/node-v7.7.4-linux-x64/bin/:$PATH`

`npm install`

3、Build

`npm run build`

# 服务端

[查看详情](https://bitbucket.org/msgbus/parkinglock_server/overview)